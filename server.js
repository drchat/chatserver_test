var io = require('socket.io').listen(app);
/*
 * IIS8からWebSocket使えるらしいので
 */
io.configure(function () {
  io.set("transports", ["xhr-polling"]);
  io.set("polling duration", 10);
});
 
app.listen(process.env.PORT || 8080, function() {
  console.log("Express server listening on port %d", app.address().port);
});
 
/*
 * Namespase「room1」用
 */
var room1 = io.of('/room1').on('connection', function(socket) {
  console.log('room1 connected');
 
  socket.on('put msg', function(data) {
    data.puttime = getPushTime();
    data.roomIndex  = 0;
    socket.json.emit('push msg', data);
    socket.json.broadcast.emit('push msg', data);
  });
});
/*
 * Namespase「room2」用
 */
var room2 = io.of('/room2').on('connection', function(socket) {
  console.log('room2 connected');
 
  socket.on('put msg', function(data) {
    data.puttime = getPushTime();
    data.roomIndex  = 1;
    socket.json.emit('push msg', data);
    socket.json.broadcast.emit('push msg', data);
  });
});
/*
 * 現在日時取得（GMT）
 */
function getPushTime() {
  var now = new Date();
  return now.getFullYear() + "/" + (now.getMonth() + 1) + "/"
      + now.getDate() + " " + now.getHours() + ":" + now.getMinutes();
}
